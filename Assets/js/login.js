const Login = {
    correctInfo : false,
    currentUser: '',
    checkUser: function() {
        let loginBtn      = document.getElementById('login-btn');
        let emailIntput   = document.getElementById('username-inp');
        let passwordInput = document.getElementById('pass-inp');        
        loginBtn.addEventListener('click',function(){
            let output = '';
            if((emailIntput.value.trim() !== '') && (passwordInput.value.trim() !== '')) {
                for(let i = 0; i < Users.usersArr.length; i++) {
                    if(Users.usersArr[i].email == emailIntput.value && Users.usersArr[i].password == passwordInput.value) {
                        output = true;
                        Login.correctInfo = true; 
                        Login.currentUser = Users.usersArr[i];                     
                        Login.loginUser();
                        break
                    }else {
                        output = false;
                    }
                }
                if(output == false) {
                    console.log('Email or PAssword is wrong');
                }               
            }else {
                alert("Please Fill Email and Password Input!");
            }
        });
    },
    loginUser: function() {
        if(Login.correctInfo) {
            General.saveUser(Login.currentUser);
            window.location.href = 'index.html';
        }
    }
}

Login.checkUser()