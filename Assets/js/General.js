const General = {
    user: '',
    saveUser: function(user) {
        if(localStorage.getItem('user')) {
            General.user = JSON.parse(localStorage.getItem('user'));
        }else {
            localStorage.setItem('user',JSON.stringify(user));
        }
    },
    loggedUser: function() {
        return JSON.parse(localStorage.getItem('user'));
    },
    setItem: function(data) {
        localStorage.setItem('user',JSON.stringify(data));
    }
}