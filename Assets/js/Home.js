
let name = document.getElementById('header__user-name');
name.innerHTML = General.loggedUser().firstName;


const Home = {
    postContainer: document.getElementById('post--container'),
    friendsList: document.querySelector('.friend--list'),
    logout: function() {
        localStorage.removeItem('user');
        window.location.href = 'loginpage.html'
    },
    addPost: function() {
        let postTextarea = document.getElementById('postebi');
        if(postTextarea.value.trim()) {
            let user = General.loggedUser();
            let posts = user.posts;
            let fullName = user.firstName +' '+user.lastName;
            let url = General.loggedUser().url;
            posts.push({author:fullName,date:new Date().toLocaleString(),image:url,text:postTextarea.value,like:Math.floor(Math.random()*(100-10+1)+10)});
            General.setItem(user);  
            Home.renderPost();  
            postTextarea.value = '';       
        }
    },
    renderPost: function() {
        let post = document.createElement('div');
        post.classList.add('postblock');
        let totalPost = JSON.parse(localStorage.getItem('user')).posts;
        let fullName = General.loggedUser().firstName +' '+General.loggedUser().lastName;
        let url = General.loggedUser().url;
        Home.postContainer.innerHTML = '';

         for(let i = 0; i < totalPost.length; i++) {
            // console.log(totalPost.length)
            post = post.cloneNode(true);
            post.innerHTML = `
            <div class="authorblock">
            <img class="pic" src="${url}">
            <span class="postauthor">${fullName} <br>
            <span class="dateBlock">${totalPost[i].date}</span>
            </span>
            </div>
            <div class="postContent"> ${totalPost[i].text}</div>
            <div class="reactSection"><span class="likes">${totalPost[i].like}</span></div>
            <div class="post--remote">
            <ul>
                <li class="list__item">
                    <i class="fas fa-thumbs-up"></i>  
                    <span> Like</span> 
                </li>
                <li class="list__item">
                    <i class="far fa-comment-alt"></i>
                    <span> comment</span>
                </li>
                <li class="list__item">
                    <i class="fas fa-share"></i>
                    <span> Share</span>
                </li>
            </ul>
            </div>`;            
            Home.postContainer.appendChild(post);
         }
    },
    renderFriends: function() {
        let totalFriend = General.loggedUser().friends;
        let item = document.createElement('li'); 
        item.classList.add('list__item', 'friend--list__item', 'f-s-b');
        for(let i = 0; i < totalFriend.length; i++) {
              item = item.cloneNode(true);
              if(totalFriend[i].online) {
                item.innerHTML = `
                <span>${totalFriend[i].firstName+' '+totalFriend[i].lastName}</span>
                <div class="active--btn"></div>
                `; 
              }else {
                item.innerHTML = `<span>${totalFriend[i].firstName+' '+totalFriend[i].lastName}</span>`;
              }           
              Home.friendsList.appendChild(item);
        }

      
    }
}

Home.renderPost();
Home.renderFriends();

let logout = document.getElementById('logout');
logout.addEventListener('click',Home.logout);

let postBtn = document.getElementById('post');
postBtn.addEventListener('click',Home.addPost);
